import uniFly from 'unifly'
import utils from '../common/utils.js'
import store from '../store'
//基础路由
uniFly.baseUrl = store.getters.baseApiUrl
//APICloud签名校验
let now = Date.now()
let appId = store.getters.appId
let appKey = store.getters.appKey
appKey = utils.SHA1(appId + 'UZ' + appKey + 'UZ' + now) + '.' + now
//设置请求头
uniFly.headers['X-APICloud-AppId'] = appId
uniFly.headers['X-APICloud-AppKey'] = appKey
//全局请求超时时间
uniFly.timeOut = 20000
//自定义请求拦截
uniFly.requestInterceptors.success = function(request) {
  console.log('自定义请求拦截')
  //配置基本信息
  request.headers = uniFly.headers
  uni.showLoading()
  return request
}
uniFly.responseInterceptors.success = function(request) {
  console.log('自定义响应拦截')
  uni.hideLoading()
  return Promise.resolve(request)
}

//所有的接口请在此处统一定义
const $api = {
  //post方法测试
  postTest: function(data) {
    return uniFly.post({
      url: 'model',
      params: data
    })
  },
  //get方法测试
  getTest: function(data) {
    return uniFly.get({
      url: 'info/5ca854ca82de0949287feeed',
      params: data
    })
  }
}

export default $api
