import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

//全局注册api模块
import api from './api/api'
Vue.prototype.$api = api

import store from './store'
Vue.prototype.$store = store

const app = new Vue({
  ...App
})
app.$mount()
